package com.bunhann.explicitintentdataextras;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class RecievedIntentExtraActivity extends AppCompatActivity {
    private TextView tvExtra;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recieved_intent_extra);

        tvExtra = (TextView) findViewById(R.id.txtExtra);

        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        String data = bundle.getString(MainActivity.EXTRA_DATA);
        tvExtra.setText(data);

    }
}
