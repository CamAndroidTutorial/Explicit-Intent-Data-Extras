package com.bunhann.explicitintentdataextras;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private TextInputEditText edData;
    private Button btnIntentData, btnIntentExtra;

    public final static String EXTRA_DATA = "DATA_EXAMPLE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edData = (TextInputEditText) findViewById(R.id.editTextData);
        btnIntentData = (Button) findViewById(R.id.btnIntentData);
        btnIntentExtra = (Button) findViewById(R.id.btnIntentExtra);

        /*
         *  Intent with extra to ReceivedIntentExtraActivity
         * */
        btnIntentExtra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edData.getText().toString().length() > 0) {
                    Intent intent = new Intent(v.getContext(), RecievedIntentExtraActivity.class);
                    intent.putExtra(EXTRA_DATA, edData.getText().toString());
                    startActivity(intent);
                } else {
                    Snackbar snackbar = Snackbar
                            .make(v, "Enter Text to continue!", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }

            }
        });
        /*
         *  Intent with data to ReceivedIntentDataActivity
         * */
        btnIntentData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), RecievedIntentDataActivity.class);
                intent.setData(Uri.parse("https://www.google.com/"));
                startActivity(intent);
            }
        });

    }
}
